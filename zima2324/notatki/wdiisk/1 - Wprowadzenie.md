# Wprowadzenie do informatyki i systemów komputerowych - wykład 1

Prowadzący będzie nawracał na Linuxa

To tylko przystawka - pełny temat zająłby 5-10 lat

Prowadzący: Joachim J. Włodarz (jjw@us.edu.pl)

Był chemikiem kwantowym który po prostu dużo pracował na kompach aż został informatykiem - nie dziw że linuksiarz

Dużo dygresji

Koncepcja języka programowania - zmiana algorytmu na coś, co komputer jest w stanie wykonać, czyli program

Paradygmaty - metody klasyfikacji cech języków, żeby określić ich mocne i słabe strony

Abstrakcja i modelowanie - odzwierciedlanie złożonych elementów w kodzie

Python - dynamicznie typowany język - ogarniał infrastrukturę????

Nasz opiekun roku był studentem p. Włodarza?

Wirtualizacja - pozwala na ustandaryzowanie programów do różnych środowisk poprzez emulowanie innych

Systemy rozproszone i chmurowe - możliwość rozłożenia przeliczeń i przetwarzania na sieć urządzeń

Jak urządzenie ma być maksymalnie wydajne, to nie dajemy z reguły OS - tylko podstawowe oprogramowanie systemowe

System operacyjny pozwala po prostu na łatwiejsze dostosowanie do użytkownika i różnych funkcji

Chcieli skapitalizować moc obliczeniową i zrobić z niej kolejny rachunek

Sztuczna inteligencja - w praktyce to po prostu uczenie maszynowe

Dzięki hipisom z lat 60. mamy ruch open source i Projekt GNU (GNU is Not UNIX) jako otwarto-źrodłowa alternatywa dla UNIXa

A potem w latach 90. powstał Freax - dzisiejszy Linux, który miał inne jądro ale na początku opierał się na GNU

System rekordów ułatwia odczytanie plików - niskopoziomowe sprawy

System bazodanowy - można zaprogramować własny na plikach i rekordach, ale obecnie mamy SQL

Będą ćwiczenia z obliczeń numerycznych i symbolicznych w Pythonie/Jupyterze, logiki cyfrowej w Logisimie, progrmowania w C, programowania nisko poziomowego i baz danych (np. w SQL)

Używanie systemów unixowych, oprogramowanie aplikacyjne, automatyzacja rutynowych czynności, publikacje i prezentacje

Literatura:
- przede wszystkim dokumentacja
- odfiltrować przestarzałe dane
- A.Kisielewicz, *Wprowadzenie do informatyki*
- A.S. Tanenbaum *strukturalna organizacja systemów komputerowych*
- P. Fulmański *Wstęp do informatyki* (dost. online)
- W. Duch *Fascynujący świat komputerów* - [Link tutaj](http://www.is.umk.pl/~duch/book-fsk.html)
- I. Wienand *Computer Science From the Bottom Up* - [Link tutaj](http://www.bottomupcs.com)

robił stronę w vimie i nawet nie ma css'a omg

Kolos będzie, trzeba się systematycznie uczyć

Następny wykład będzie konkretniejszy