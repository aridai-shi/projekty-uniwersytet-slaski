## Projekty UŚ / PL

Projekty programistyczne wszelkiego rodzaju, pisane na potrzeby zajęć na Uniwersytecie Śląskim. Organizowane wg. semestrów a w środku wg. rodzajów. Repozytorium jest w większości prowadzone po polsku. Cały kod tutaj jest dostępny do użytku na warunkach licencji The Unlicense, chyba że określono inaczej w komentarzach lub w dalszej części tego pliku.

## UŚ Projects / EN

Programming projects of all sorts, written for my studies at the University of Silesia. Organized by term, and internally by type. This repository is mostly run in Polish. All of the code here is available for use on the terms of The Unlicense, unless specified otherwise via comments or a latter part of this file.